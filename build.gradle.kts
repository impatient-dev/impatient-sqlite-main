plugins {
	id("org.jetbrains.kotlin.jvm")
}

group = "imp-dev"
version = "0.0.0"

repositories {
	mavenCentral()
}

dependencies {
	api(project(":impatient-sqlite"))
	api(project(":impatient-sqlite-jdbc"))
	implementation(project(":impatient-utils"))

	api("org.xerial:sqlite-jdbc:3.40.1.0")

	testImplementation(project(":impatient-junit4"))
	testImplementation(project(":impatient-logback"))
	testImplementation(project(":impatient-orm-kotlin"))
}