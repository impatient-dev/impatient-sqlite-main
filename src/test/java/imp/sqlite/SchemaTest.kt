package imp.sqlite

import imp.logback.initLogbackTrace
import imp.orm.kt.*
import imp.orm.kt.impl.SchemaHandler
import imp.sqlite.main.Sqlite
import imp.util.logger
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.reflect.KClass

private data class A(
	@PrimaryKey var id: Long = -1L,
	var name: String,
)
@OrmTable("A") private data class ANull(
	@PrimaryKey var id: Long = -1L,
	var name: String?,
)

private data class R(
	@PrimaryKey var id: Long = -1L,
	@References("A") var aId: Long,
)
@OrmTable("R") private data class RNoFk(
	@PrimaryKey var id: Long = -1L,
	var aId: Long,
)

@OrmIndex("S_index", "aId")
private data class S(
	@PrimaryKey var id: Long = -1L,
	@References("A") var aId: Long,
	@References("R") var rId: Long?,
)
@OrmIndex("S_index", "aId, rId")
@OrmTable("S") private data class SNoFk(
	@PrimaryKey var id: Long = -1L,
	var aId: Long?,
	var rId: Long,
)

/**Verifies we can apply schemas correctly, even on top of other schemas.*/
class SchemaTest {
	@Test fun test() = Sqlite.inMemory().use { db ->
		initLogbackTrace()
		val log = SchemaTest::class.logger
		assertSchema(db) // 0 tables

		log.info("Testing with 1 simple table.")
		val a = Orm(listOf(A::class))
		a.applySchema(db)
		assertSchema(db, a)

		log.info("Testing with a foreign key added.")
		val r = Orm(listOf(A::class, R::class))
		r.applySchema(db)
		assertSchema(db, r)

		log.info("Testing with R removed.")
		a.applySchema(db)
		assertSchema(db, a)

		log.info("Testing: R without FKs, S with FKs referencing A and R")
		val s_rNoFk = Orm(listOf(A::class, RNoFk::class, S::class))
		s_rNoFk.applySchema(db)
		assertSchema(db, s_rNoFk)

		log.info("Testing: add FKs to R, change S, make A.name nullable")
		val s_aNull = Orm(listOf(ANull::class, R::class, SNoFk::class))
		s_aNull.applySchema(db)
		assertSchema(db, s_aNull)
	}
}


private fun assertSchema(db: Database, orm: Orm) {
	val actual = SchemaHandler.read(db)
	val expected = orm.schema
	assertEquals(expected, actual)
}

private fun assertSchema(db: Database, vararg tables: KClass<out Any>) = assertSchema(db, Orm(tables.toList()))