package imp.sqlite.main;

import imp.sqlite.jdbc.JdbcDatabase;
import org.sqlite.SQLiteConfig;

import java.nio.file.Path;
import java.sql.DriverManager;
import java.sql.SQLException;

/**Class that connects to SQLite databases for you.*/
public class Sqlite
{
	static
	{
		try{Class.forName("org.sqlite.JDBC");}//recommended at bitbucket.org/xerial/sqlite-jdbc
		catch(ClassNotFoundException e){throw new RuntimeException(e);}
	}

	private static SQLiteConfig config()
	{
		SQLiteConfig out = new SQLiteConfig();
		out.enforceForeignKeys(true);
		return out;
	}

	/**Connects to a new in-memory database. Foreign key enforcement will be enabled for this connection.*/
	public static JdbcDatabase inMemory()
	{
		try {
			return new JdbcDatabase(DriverManager.getConnection("jdbc:sqlite::memory:", config().toProperties()));
		} catch(SQLException e){throw new RuntimeException(e);}
	}

	/**Connects to a database in an SQLite file. The file does not have to exist yet, but the directory containing it must.
	 * Foreign key enforcement will be enabled for this connection.*/
	public static JdbcDatabase file(String path)
	{
		try {
			return new JdbcDatabase(DriverManager.getConnection("jdbc:sqlite:" + path, config().toProperties()));
		} catch(SQLException e){throw new RuntimeException(e);}
	}

	/**Connects to a database in an SQLite file. The file does not have to exist yet, but the directory containing it must.
	 * Foreign key enforcement will be enabled for this connection.*/
	public static JdbcDatabase file(Path path)
	{
		return file(path.toString());
	}
}
