# impatient-sqlite-main

This is the SQLite project that knows how to actually create a (JDBC) SQLite database, and that contains tests for our SQLite mapper.
This is the project you should actually use when working with SQLite (unless you're on Android).